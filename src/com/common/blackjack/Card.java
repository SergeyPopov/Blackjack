package com.common.blackjack;


	public class Card {

	    private Suit mSuit;
	    private Rank mRank;

	    public Card(Suit suit, Rank rank) {
	        this.mSuit = suit;
	        this.mRank = rank;
	    }

	    public Suit getSuit() {
	        return mSuit;
	    }

	    public Rank getRank() {
	        return mRank;
	    }

	    public int getValue() {
	       int value = 0;
	       value = mRank.ordinal() + 2;
	       if (mRank.ordinal()>8){
	    	   value = 10;
	       }
	       if (mRank.ordinal()==12){
	    	   value = 11;
	       }
	       
	       return value;
	     }

	    @Override
	    public boolean equals(Object o) {
	        return (o != null && o instanceof Card && ((Card) o).mRank == mRank && ((Card) o).mSuit == mSuit);
	    }
	    
	    @Override
	    public String toString() {
	         return ("Card's suit:"+this.getSuit()+
	                 " Card's rank: "+ this.getRank() +
	                 " Card's value: "+ this.getValue());
	    }

 }
