package com.common.blackjack;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.stream.Collectors;

public class DealerConsole {
	
	private static ArrayList<Player> Players = new ArrayList<Player>();
	private static ArrayList<String> badloosers = new ArrayList<String>();
	private static CardsDeck playerdesk;
		
    
	public static Integer getLocksValue(ArrayList<Card> Lock) {
      Integer lvalue = 0; 
      Card card;
      
      for(int i = 0; i < Lock.size(); i++) {   
    	  card = Lock.get(i);
    	  lvalue = lvalue + card.getValue();
    	} 
      
      return lvalue;
        
    }
	public static ArrayList<Player> getPlayers() {
		return Players;
	}
    
	
    public static boolean isNameValid(String s) {
    	boolean inputcorrect = false;
    	if (s.trim().length() == 0){
    		inputcorrect =false;
    	} else{
    		inputcorrect =true;
    	}
    	return inputcorrect;
    }	
	
	
    public static void setPlayers(int np ) {
    	BufferedReader br = null;
	    boolean inputcorrect  = false;
    	try {
          for(int i = 0; i < np; i++) {   
          	  Player player = new Player();
      	      br = new BufferedReader(new InputStreamReader(System.in));
		      
		      
	          while (!inputcorrect) {
	      	      System.out.print("Please enter the UNIQUE player's name : ");
	      	      String Name = br.readLine();
		         
	      	      inputcorrect = isNameValid(Name);
		          
	      	      if (inputcorrect){
	      	    	     player.setName(Name);
		          }else{
		            	 System.out.println("Please type in the meaningful name");
		          }
		      }
		      
	          inputcorrect  = false;
		      
		      player.setPlayerdesk(new CardsDeck());
		     
		      if(i==0){
		         player.setType("human");
		      }else{
		    	 player.setType("computer"); 
		      }
		      
		      if(i==np-1){
			         player.setRole("dealer");
			      }else{
			    	 player.setRole("player"); 
			  }
		      //for test only
		      player.toString();
		      
		      Players.add(player);
       	  } 
    	} catch (IOException e) {
	  	   e.printStackTrace();
		}
    }
    
    
    public static void hitPlayer(CardsDeck newDeck, CardsDeck iplayerdesk, Integer idx, boolean ishit) {
  		ArrayList<Card> iPulledCards = new ArrayList<Card>();
  		Player iplayer = Players.get(idx);
    	try {
       	 	 iPulledCards = iplayer.getPlayerdesk().getmPulledCards();
       	 	 Card card = newDeck.pullRandom();
       	      
       	 	 //for test only 
       	 	  if (ishit){
       	 	    System.out.println("Adding " + card.toString() +" to " + iplayer.getName());
       	 	  }
       	      iPulledCards.add(card) ;
       	 	  iplayerdesk.setmPulledCards(iPulledCards);
       		  iplayer.setPlayerdesk(iplayerdesk) ;
       		 
       		 Players.set(idx, iplayer);
       	} catch (Exception e) {
	  	   e.printStackTrace();
		}
    }
    
    
    public static void initPlayers(CardsDeck newDeck) {
    	Integer value = 0;
    	CardsDeck iplayerdesk = new CardsDeck();
    	
    	try {
    	  for(int j = 0; j < 2; j++) {   
    		System.out.println("Initiating round " + j);
    		iplayerdesk = new CardsDeck();
    		DealCards(newDeck, iplayerdesk, 0, false);
    	  }	
    	  
    	  // initial hands check
    	  value = Players.get(0).getPlayerdesk().getLocksValue();
    	  if (value==21){
    		System.out.println("Player got 21! Human player (You) wins!" );
          	System.exit(0);
    	  }
    	  
    	  if (value ==22){
    		  if (Players.get(Players.size()-1).getPlayerdesk().getLocksValue()==22){
    	    	Players.get(Players.size()-1).getPlayesrHand();
    	    	System.out.println("You and Dealer got 22! Dealer (computer) wins!" );
    	    	System.exit(0);
    		  }
    	  }
    	  
    	  
    	} catch (Exception e) {
	  	   e.printStackTrace();
		}
    }
    
    public static void DealCards(CardsDeck newDeck, CardsDeck iplayerdesk, Integer idx, boolean ishit) {
    	try {
    		iplayerdesk = new CardsDeck();
    		for(int i = idx; i < Players.size(); i++) {  
    			if (ishit){
    			    System.out.println("Deal card to the player : " + Players.get(i).getName());
    			}
    			Player iplayer = Players.get(i);
           	    if (ishit){
          	      if (Players.get(i).getPlayerdesk().getLocksValue()  < 17){
          	    	 hitPlayer(newDeck, iplayerdesk, i, ishit);
                  } else {
                	  System.out.println(Players.get(i).getName() + " passed" );
                  }
      		    }else{
      		    	hitPlayer(newDeck, iplayerdesk, i, ishit);
      		    }
        	    //test only
        	    //Players.get(i).getPlayesrHand();
        	    iplayerdesk = new CardsDeck();
        	    
        	    if (ishit){
        	      if (Players.get(i).getPlayerdesk().getLocksValue()  > 21){
                	  System.out.println("Player " + Players.get(i).getName() + " got over 21! Player " + i + " lost!" );
                	  //loosers.add(i);
                	  badloosers.add(Players.get(i).getName());
                  }
    		    }
        	    
       	    }
    	} catch (Exception e) {
	  	   e.printStackTrace();
		}
    }
    
    
    
    public static void Stay(CardsDeck newDeck, CardsDeck iplayerdesk, Integer idx, boolean ishit) {
    	boolean passed = false;
    	try {
    		iplayerdesk = new CardsDeck();
    		for(int i = idx; i < Players.size(); i++) {  
    			if (ishit){
    			    System.out.println("Deal card to the player : " + Players.get(i).getName());
    			}
           	    
         	    while (!passed){

               	    if (ishit){
                	      if (Players.get(i).getPlayerdesk().getLocksValue()  < 17){
                	    	 hitPlayer(newDeck, iplayerdesk, i, ishit);
                        } else {
                      	  System.out.println(Players.get(i).getName() + " passed" );
                      	  passed = true;
                        }
            		    }else{
            		    	hitPlayer(newDeck, iplayerdesk, i, ishit);
            		 }
         	    	
         	    }
         	    passed = false;
    			//test only
        	    //Players.get(i).getPlayesrHand();
        	    iplayerdesk = new CardsDeck();
        	    
        	    if (ishit){
        	      if (Players.get(i).getPlayerdesk().getLocksValue()  > 21){
                	  System.out.println("Player " + Players.get(i).getName() + " got over 21! Player " + i + " lost!" );
                	  //loosers.add(i);
                	  badloosers.add(Players.get(i).getName());
                  }
    		    }
        	    
       	    }
    	} catch (Exception e) {
	  	   e.printStackTrace();
		}
    }
    
    
     public static void removeLoosers() {
        //test only 
    	//System.out.println(badloosers.toString()+ " " +Players.size());
    	try {
    		for(int i = 0; i < badloosers.size(); i++) {  
        	    String Name = badloosers.get(i);
        	    Players.removeIf(s-> s.getName().contains(Name));
    			System.out.println("Player " + Name + " lost and leaving the table");
       	    }
    		badloosers.removeAll(badloosers);		
            //test only 
    		//System.out.println(" " + Players.size() +" "+badloosers.size());	
    	} catch (Exception e) {
	  	   e.printStackTrace();
		}
    }

     public static void getWinners() {

     	try {
     	   
     		//Java8 
     		Collections.sort(Players, (p1, p2) -> p1.getPlayerdesk().getLocksValue().compareTo(p2.getPlayerdesk().getLocksValue()));
            
     		Comparator<Player> ByRole = (p1, p2) -> p1
     	            .getRole().compareTo(p2.getRole());

     	    Comparator<Player> ByType = (p1, p2) -> p1.getType()
     	            .compareTo(p2.getType());

     	   // Players.stream().sorted(ByRole.thenComparing(ByType))
     	   //         .forEach(p -> System.out.println(p));
     		
     		
     		//----------------------------------------------------------------------------------------
     		/*
     		Players
               .stream()
               .sorted((p1, p2) -> p1.getPlayerdesk().getLocksValue()
                        .compareTo(p2.getPlayerdesk().getLocksValue()))
               .forEach(p -> System.out.println(p));
     		 */
             //----------------------------------------------------------------------------------------
     		 
     		 //Comparator<Player> byByPlayersHand = (Player p1, Player p2) -> Integer
             //       .compare(p1.getPlayerdesk().getLocksValue(), p2.getPlayerdesk().getLocksValue());
             //----------------------------------------------------------------------------------------
     		for(int i = 0; i < Players.size(); i++) {  
        		 Players.get(i).getPlayesrHand();
        	 	 
       	     }
     		 System.out.println(Arrays.asList(Players));

     		 /* Java7
     		 Collections.sort(Players);
        	 System.out.println(Arrays.asList(Players));
       		 
        	 for(int i = 0; i < Players.size(); i++) {  
        		 Players.get(i).getPlayesrHand();
        	 	 
       	     }
        	 */
        	 System.out.println(Players.get(Players.size()-1).getName()+ "," + Players.get(Players.size()-1).getType()+ "," + Players.get(Players.size()-1).getRole() +", is the winner!");
        	 System.exit(0);
        	 
     	} catch (Exception e) {
 	  	   e.printStackTrace();
 		}
     }
 
     
    public static boolean isInteger(String s) {
    	    return isInteger(s,10);
    }

    public static boolean isInteger(String s, int radix) {
    	    if(s.isEmpty()) return false;
    	    for(int i = 0; i < s.length(); i++) {
    	        if(i == 0 && s.charAt(i) == '-') {
    	            if(s.length() == 1) return false;
    	            else continue;
    	        }
    	        if(Character.digit(s.charAt(i),radix) < 0) return false;
    	    }
    	    int n = Integer.parseInt(s);
    	    if(n < 2 || n > 5 ) return false;
    	    return true;
   	}
     
    public static void main(String[] args) {

	        BufferedReader br = null;
		    Card card;
		    boolean isdealerpulled = false;
		    boolean inputcorrect  = false;
		    int np = 0;
		    try {

	            br = new BufferedReader(new InputStreamReader(System.in));
	            MainDeck newDeck = new MainDeck();
	            
	            while (!inputcorrect) {
	               System.out.print("Enter number of players (total with Dealer, 2-5) : ");
	               String nplayers = br.readLine();
	               inputcorrect = isInteger(nplayers);
	               if (inputcorrect){
	                     np = Integer.parseInt(nplayers);
	               }else{
	            	    System.out.println("Input must be the number between 2 and 5");
	               }
	            }
	           
	            setPlayers(np);
	            initPlayers(newDeck);
	            
	            //--------------------------------------------------------------
	            
	            Players.get(0).getPlayesrHand();
	            
                while (true) {
	 
                	// choose your action
	                System.out.print(Players.get(0).getName() + ",please enter your choice as h(pull), s(stay), o(open), c(check): ");
	                String input = br.readLine();
	                //validate your action
	                System.out.println("input : " + input);
	                System.out.println("-----------\n");          	
	                
	                // checking your hand
	                if ("c".equals(input)) {
	                	Players.get(0).getPlayesrHand();
	                }

	                //you decided to hit
	                if ("h".equals(input)) {
	                	CardsDeck iplayerdesk = new CardsDeck();
	                	hitPlayer(newDeck, iplayerdesk, 0, true);
	                	
	            	    if (Players.get(0).getPlayerdesk().getLocksValue()  > 21){
		                	System.out.println("Player got over 21! Player lost!" );
		                	System.exit(0);
		                }
	                
	                }
                    
	                //you decided to stay - dealer's turn
	                if ("s".equals(input)) {
	                	isdealerpulled = true;
	                	CardsDeck iplayerdesk = new CardsDeck();
	                	Stay(newDeck, iplayerdesk, 1, isdealerpulled);
	                	removeLoosers();

	                }

	                //opening hands, only if dealer has pulled (or decided not pull)
	                if ("o".equals(input)) {
	                	
	                  if(isdealerpulled){	
	                	 getWinners();
	                  }else{
	                	  System.out.println("Please hit others, use stay option!");
	                  }
	                }
	                
	                //end of game
	                if ("q".equals(input)) {
	                    System.out.println("Exit!");
	                    System.exit(0);
	                }

	            }

	        } catch (IOException e) {
	            e.printStackTrace();
	        } finally {
	            if (br != null) {
	                try {
	                    br.close();
	                } catch (IOException e) {
	                    e.printStackTrace();
	                }
	            }
	        }

	    }
	}

