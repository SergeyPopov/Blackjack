package com.common.blackjack;

import java.util.ArrayList;
import java.util.Random;

public class MainDeck extends CardsDeck{
	    private ArrayList<Card> mCards;
	    private ArrayList<Card> mPulledCards;
	    private Random mRandom;


	    public MainDeck() {
	        mRandom = new Random();
	        mPulledCards = new ArrayList<Card>();
	        mCards = new ArrayList<Card>(Suit.values().length * Rank.values().length);
	        reset();
	    }

	    public void reset() {
	        mPulledCards.clear();
	        mCards.clear();
	        /* Creating all possible cards... */
	        for (Suit s : Suit.values()) {
	            for (Rank r : Rank.values()) {
	                Card c = new Card(s, r);
	                mCards.add(c);
	            }
	        }
	    }

	    
	    /**
	     * get a random card, removing it from the pack
	     * @return
	     */
	    public Card pullRandom() {
	        if (mCards.isEmpty())
	            return null;

	        Card res = mCards.remove(randInt(0, mCards.size() - 1));
	        if (res != null)
	            mPulledCards.add(res);
	        return res;
	    }

	    /**
	     * Get a random cards, leaves it inside the pack 
	     * @return
	     */
	    public Card getRandom() {
	        if (mCards.isEmpty())
	            return null;

	        Card res = mCards.get(randInt(0, mCards.size() - 1));
	        return res;
	    }
	   
	    /**
	     * Returns a pseudo-random number between min and max, inclusive.
	     * The difference between min and max can be at most
	     * <code>Integer.MAX_VALUE - 1</code>.
	     *
	     * @param min Minimum value
	     * @param max Maximum value.  Must be greater than min.
	     * @return Integer between min and max, inclusive.
	     * @see java.util.Random#nextInt(int)
	     */
	    public int randInt(int min, int max) {
	        // nextInt is normally exclusive of the top value,
	        // so add 1 to make it inclusive
	        int randomNum = mRandom.nextInt((max - min) + 1) + min;
	        return randomNum;
	    }


	    public boolean isEmpty(){
	        return mCards.isEmpty();
	    }
	    

	    public  Integer getLocksValue() {
	        Integer lvalue = 0; 
	        Card card;
	        
	        for(int i = 0; i < this.mPulledCards.size(); i++) {   
	      	  card = this.mPulledCards.get(i);
	      	  lvalue = lvalue + card.getValue();
	      	} 
	        
	        return lvalue;
	          
	      }
	    
}
