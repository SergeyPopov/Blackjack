package com.common.blackjack;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;

import org.junit.Test;

public class Player implements Comparable<Player>{

	private String    Name;
	private String    Role;
	private String    Type;
	private boolean   ishit;
	private Integer   valueOnHand;
	private CardsDeck playerdesk;
	
	public String getName() {
		return Name;
	}
	public void setName(String name) {
		Name = name;
	}
	public String getRole() {
		return Role;
	}
	public void setRole(String role) {
		Role = role;
	}

	public boolean isIshit() {
		return ishit;
	}
	public void setIshit(boolean ishit) {
		this.ishit = ishit;
	}
	public String getType() {
		return Type;
	}
	public void setType(String type) {
		this.Type = type;
	}
	public CardsDeck getPlayerdesk() {
		return playerdesk;
	}
	public void setPlayerdesk(CardsDeck playerdesk) {
		this.playerdesk = playerdesk;
	}
	
	public void hit(Card card) {
  		ArrayList<Card> iPulledCards = new ArrayList<Card>();
  		CardsDeck playerdesk = new CardsDeck();
    	try {
       	 	 iPulledCards = this.getPlayerdesk().getmPulledCards();
      	 	 System.out.println("Adding " + card.toString() +" to " + this.getName());
       	     iPulledCards.add(card) ;
       	     playerdesk.setmPulledCards(iPulledCards);
       		 this.setPlayerdesk(playerdesk) ;
       	} catch (Exception e) {
	  	   e.printStackTrace();
		}
    }
	
	public Integer getValueOnHand() {
		  //for test only
		  //System.out.println("Player have cards : " + this.playerdesk.getmPulledCards().size());
		  valueOnHand = 0;
		  for(int i = 0; i < this.playerdesk.getmPulledCards().size(); i++) {   
			  Card card = this.playerdesk.getmPulledCards().get(i);
			  //for test only
			  //System.out.println("Player have got : " + card.getRank() + " of " +card.getSuit());
	    	  valueOnHand = valueOnHand + card.getValue();
	      } 
		return valueOnHand;
	}

	public void setValueOnHand(Integer valueOnHand) {
		this.valueOnHand = valueOnHand;
	}

	public void getPlayesrHand() {
		System.out.println(this.getName()+"'s" + " hand");
		System.out.println("-------------------------------------------------");
		try{
  		   for(int i = 0; i < this.playerdesk.getmPulledCards().size(); i++) {   
		    	  Card card = this.playerdesk.getmPulledCards().get(i);
		    	  System.out.println("Player have got : " + card.getRank() + " of " +card.getSuit());
		   } 
  		   System.out.println("-------------------------------------------------");
	       System.out.println("Lock value : " + (this.playerdesk.getLocksValue()));
	       System.out.println("\n");
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public String toString() {
        return ("( Hand " + this.getPlayerdesk().getLocksValue() + ", " + Name + ", " +  Role + ", " + Type + ")");
    }
	
    @Override
	    public int compareTo(Player o) {
	        return toString().compareTo(o.toString());
	    }

}
