package com.common.blackjack;

import java.util.ArrayList;
import java.util.Random;

public class CardsDeck {
    private ArrayList<Card> mCards;
    private ArrayList<Card> mPulledCards = new ArrayList<Card>();
	private Random mRandom;
    
    public ArrayList<Card> getmCards() {
		return mCards;
	}

	public void setmCards(ArrayList<Card> mCards) {
		this.mCards = mCards;
	}

	public ArrayList<Card> getmPulledCards() {
		return mPulledCards;
	}

	public void addToPulledCards(Card card) {
		mPulledCards.add(card);
	}
	

	public void setmPulledCards(ArrayList<Card> mPulledCards) {
		this.mPulledCards = mPulledCards;
	}


    public void reset() {
        mPulledCards.clear();
        mCards.clear();
        /* Creating all possible cards... */
        for (Suit s : Suit.values()) {
            for (Rank r : Rank.values()) {
                Card c = new Card(s, r);
                mCards.add(c);
            }
        }
    }

    public void clear() {
        this.mPulledCards = new ArrayList<Card>();
        this.mCards = new ArrayList<Card>() ;
    	//this.mPulledCards.clear();
        //this.mCards.clear();
    }
    
    /**
     * get a random card, removing it from the pack
     * @return
     */
    public Card pullRandom() {
        if (mCards.isEmpty())
            return null;

        Card res = mCards.remove(randInt(0, mCards.size() - 1));
        if (res != null)
            mPulledCards.add(res);
        return res;
    }

    public void addToHand(Card card) {
        if (card != null)
            this.mPulledCards.add(card);
    }
    
    /**
     * Get a random cards, leaves it inside the pack 
     * @return
     */
    public Card getRandom() {
        if (mCards.isEmpty())
            return null;

        Card res = mCards.get(randInt(0, mCards.size() - 1));
        return res;
    }
   
    /**
     * Returns a pseudo-random number between min and max, inclusive.
     * The difference between min and max can be at most
     * <code>Integer.MAX_VALUE - 1</code>.
     *
     * @param min Minimum value
     * @param max Maximum value.  Must be greater than min.
     * @return Integer between min and max, inclusive.
     * @see java.util.Random#nextInt(int)
     */
    public int randInt(int min, int max) {
        // nextInt is normally exclusive of the top value,
        // so add 1 to make it inclusive
        int randomNum = mRandom.nextInt((max - min) + 1) + min;
        return randomNum;
    }


    public boolean isEmpty(){
        return mCards.isEmpty();
    }
    

    public  Integer getLocksValue() {
        Integer lvalue = 0; 
        Card card;
        
        for(int i = 0; i < this.mPulledCards.size(); i++) {   
      	  card = this.mPulledCards.get(i);
      	  lvalue = lvalue + card.getValue();
      	} 
        
        return lvalue;
          
      }
    
	public String toString() {
		String decks = null;
		try{
			   decks = "Pulled cards on hand: ";
			   decks = decks + "\n"	;	   
			   decks = decks + "----------------------------------------------------------";
			   decks = decks + "\n"	;	   
	  		   for(int i = 0; i < this.mPulledCards.size(); i++) {   
			    	  Card card = this.mPulledCards.get(i);
			    	  decks = decks + ("Player have got : " + card.getRank() + " of " +card.getSuit());
					  decks = decks + "\n"	;	   
			   } 
			}catch(Exception e){
				e.printStackTrace();
			}
		
		return decks;
    }
    
}
