package com.common.blackjack;

import java.util.Comparator;

public class PlayerComparator implements Comparator<Player> {

    @Override
    public int compare(Player o1, Player o2) {
        return o1.getPlayerdesk().getLocksValue().compareTo(o2.getPlayerdesk().getLocksValue());
    }
}
