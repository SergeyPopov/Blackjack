package com.common.blackjack;

public enum Suit {
    SPADES,
    HEARTS,
    DIAMONDS,
    CLUBS;
}
