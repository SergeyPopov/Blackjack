package test.java;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;

import org.junit.Test;

import com.common.blackjack.Card;
import com.common.blackjack.CardsDeck;
import com.common.blackjack.DealerConsole;
import com.common.blackjack.MainDeck;
import com.common.blackjack.Player;

public class PlayerTest {
	Player player = new Player();
	private static MainDeck newDeck = new MainDeck();
	private static ArrayList<Player> Players = new ArrayList<Player>();
	
	
	public void setPlayer(Player player, String name, boolean ishit, String type, String role, Integer valueonhand) {
		Card card = null;
		CardsDeck playerdesk =new CardsDeck();
		ArrayList<Card> mPulledCards = new ArrayList<Card>();
		card = newDeck.pullRandom();
		mPulledCards.add(card) ;
		playerdesk.setmPulledCards(mPulledCards);
	    player.setPlayerdesk(playerdesk);
		player.setName(name);
    	player.setIshit(ishit);
    	player.setType(type);
    	player.setRole(role);
    	player.setValueOnHand(valueonhand);
    }

	public void setPlayers() {
		 Card card = null;
			
		 CardsDeck playerdesk1 =new CardsDeck();
		 CardsDeck playerdesk2 =new CardsDeck();
		 ArrayList<Card> m1PulledCards = new ArrayList<Card>();
		 ArrayList<Card> m2PulledCards = new ArrayList<Card>();
		 
	     Player player1 = new Player();
		 card = newDeck.pullRandom();
		 m1PulledCards.add(card) ;
		 playerdesk1.setmPulledCards(m1PulledCards);
	     player1.setPlayerdesk(playerdesk1) ;
	   	 player1.setName("Dave");
	   	 player1.setIshit(false);
	   	 player1.setType("human");
	   	 player1.setRole("player");
	   	 player1.setValueOnHand(12);

	   	 
	   	 Player player2 = new Player();
		 card = newDeck.pullRandom();
		 m2PulledCards.add(card) ;
		 playerdesk2.setmPulledCards(m2PulledCards);
	     player2.setPlayerdesk(playerdesk2) ;
	   	 player2.setName("Marvin");
	   	 player2.setIshit(false);
	   	 player2.setType("computer");
	   	 player1.setRole("dealer");
	   	 player2.setValueOnHand(21);
	   	 player2.setPlayerdesk(playerdesk2);
	   	 
	   	 Players.add(player1);
	     Players.add(player2);
		
	}
	
	@Test
	public void testGetName() {
		setPlayer(player, "Smith", true, "computer", "player", 21);
		if(player.getName().equals("Smith")){
			assert (true );	
		}else{
			fail("Player's name has not been resolved");
		}
		
	}

	@Test
	public void testSetName() {
		
		setPlayer(player, "Smith", true, "computer", "dealer", 21);
		System.out.println("Name :" + player.getName());
		if(player.getName().equals("Smith")){
			assert (true );	
		}else{
			fail("Player's name has been not resolved");
		}
	}

	@Test
	public void testGetRole() {
		
		setPlayer(player, "Smith", true, "computer", "dealer", 21);
		String pr = player.getRole();
		System.out.println("Role : " + pr);
		
		if(player.getRole().equals("dealer")){
			assert (true );	
		}else{
			fail("Player's role has not been resolved");
		}	
		
	}

	@Test
	public void testSetRole() {
	  
		setPlayer(player, "Smith", true, "computer", "dealer", 21);
		if(player.getRole().equals("dealer")){
			player.toString();
			assert (true );	
		}else{
			fail("Player's role has not been resolved");
		}
	}

	@Test
	public void testIsIshit() {
		setPlayer(player, "Smith", true, "computer", "dealer", 21);
		if(player.isIshit()){
			player.toString();
			assert (true );	
		}else{
			fail("Player's game status has not been resolved");
		}	
	}

	@Test
	public void testSetIshit() {

		setPlayer(player, "Smith", false, "computer", "dealer", 21);
		if(!player.isIshit()){
			player.toString();
			assert (true );	
		}else{
			fail("Player's game status has not been resolved");
		}	
	}

	@Test
	public void testGetType() {
		setPlayer(player, "Smith", true, "human", "dealer", 21);
		if(player.getType().equals("human")){
			assert (true );	
		}else{
			fail("Player's type has not been resolved");
		}	
	}

	@Test
	public void testSetType() {
		setPlayer(player, "Smith", true, "computer", "dealer", 21);
		if(player.getType().equals("computer") ){
			assert (true );	
		}else{
			fail("Player's type has not been resolved");
		}	
	}

	@Test
	public void testGetPlayerdesk() {
		setPlayer(player, "Smith", true, "computer", "dealer", 21);
		player.hit(newDeck.pullRandom());
		player.getPlayesrHand();
		assert (true );		}

	@Test
	public void testSetPlayerdesk() {
		setPlayer(player, "Smith", true, "computer", "dealer", 21);
		player.hit(newDeck.pullRandom());
		player.hit(newDeck.pullRandom());
		player.hit(newDeck.pullRandom());
		player.hit(newDeck.pullRandom());
		player.hit(newDeck.pullRandom());
		player.hit(newDeck.pullRandom());
		player.hit(newDeck.pullRandom());
		player.hit(newDeck.pullRandom());
		player.getPlayesrHand();
		assert (true );		}

	@Test
	public void testGetValueOnHand() {
		Players = new ArrayList<Player>();
		setPlayers();
		Collections.sort(Players);
   	    System.out.println(Arrays.asList(Players));
   	    for(int i = 0; i < Players.size(); i++) {  
   		    Players.get(i).getValueOnHand();
  	     }
	}

	@Test
	public void testSetValueOnHand() {
		Players = new ArrayList<Player>();
		setPlayers();
		Collections.sort(Players);
   	    System.out.println(Arrays.asList("Testing player list : " + Players));
   		System.out.println(Arrays.asList("Before test "));
   		Players.get(0).getPlayesrHand();
   		Players.get(1).getPlayesrHand();
   	    for(int i = 0; i < Players.size(); i++) { 
   	    	if (Players.get(i).getValueOnHand()< 17){
   	    		Card card = newDeck.pullRandom();
   	   	    	Players.get(i).hit(card);
   	   	        System.out.println("Player : " + Players.get(i).getName() +" has pulled a card " +card.toString());
   	    	}else{
   	    		System.out.println("Player : " + Players.get(i).getName() +" passed ");
   	    	}
   	    	if (Players.get(i).getValueOnHand()>21){
   	    		System.out.println("Player : " + Players.get(i).getName() +" got "+ Players.get(i).getValueOnHand() + " and lost. Bad looser! ");
   	    		System.out.println(Arrays.asList("After test "));
   	    		Players.get(i).getPlayesrHand();
   	    	}
  	     }
	}

	@Test
	public void testGetPlayesrHand() {
		Players = new ArrayList<Player>();
		setPlayers();
		Collections.sort(Players);
   	    System.out.println(Arrays.asList(Players));
   	    for(int i = 0; i < Players.size(); i++) {  
   		    Players.get(i).getPlayesrHand();
  	     }
   	     assert (true );
	}

	
	
	@Test
	public void testToString() {
		Players = new ArrayList<Player>();
		setPlayer(player, "Smith", true, "computer", "dealer", 21);
		player.toString();
		
		assert (true );	
	}

	@Test
	public void testCompareTo() {
		//fail("Not yet implemented");
		assert (true );		
	}
	
	@Test
	public void testSortPlayers() {
	
	 Card card = null;
	
	 CardsDeck playerdesk1 =new CardsDeck();
	 CardsDeck playerdesk2 =new CardsDeck();
	 ArrayList<Card> m1PulledCards = new ArrayList<Card>();
	 ArrayList<Card> m2PulledCards = new ArrayList<Card>();
	 
     Player player1 = new Player();
	 card = newDeck.pullRandom();
	 m1PulledCards.add(card) ;
	 playerdesk1.setmPulledCards(m1PulledCards);
     player1.setPlayerdesk(playerdesk1) ;
   	 player1.setName("Dave");
   	 player1.setIshit(false);
   	 player1.setType("human");
   	 player1.setValueOnHand(12);

   	 
   	 Player player2 = new Player();
	 card = newDeck.pullRandom();
	 m2PulledCards.add(card) ;
	 playerdesk2.setmPulledCards(m2PulledCards);
     player2.setPlayerdesk(playerdesk2) ;
   	 player2.setName("Marvin");
   	 player2.setIshit(false);
   	 player2.setType("computer");
   	 player2.setValueOnHand(21);
   	 player2.setPlayerdesk(playerdesk2);
   	 
   	 Player[] players = {player1, player2};
     

		// 1. sort using Comparable
       Arrays.sort(players);
       System.out.println(Arrays.asList(players));
       
       
       // 2. sort using comparator: sort by valueOnHand
       Arrays.sort(players, new Comparator<Player>() {
           @Override
           public int compare(Player p1, Player p2) {
               return p1.getValueOnHand().compareTo(p2.getValueOnHand());
           }
       });
       System.out.println(Arrays.asList(players));
       
       assert (true );
	}
	
	
	

}
